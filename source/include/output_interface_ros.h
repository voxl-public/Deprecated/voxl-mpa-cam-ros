/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef OUTPUT_ROS_INTERFACE_H
#define OUTPUT_ROS_INTERFACE_H

#include <image_transport/image_transport.h>
#include <image_transport/camera_publisher.h>
#include <ros/ros.h>
#include <std_msgs/Header.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <modal_pipe_client.h>
#include <modal_pipe_interfaces.h>
#include "common_defs.h"
#include "debug_log.h"

// Forward decl
struct camera_image_metadata_t;

//------------------------------------------------------------------------------------------------------------------------------
// Ros output initialize structure
//------------------------------------------------------------------------------------------------------------------------------
struct OutputRosInitData
{
    ros::NodeHandle* pRosNodeHandle;    ///< ROS handle
    const char*      pRosTopic;         ///< Ros topic to write to
};

//------------------------------------------------------------------------------------------------------------------------------
// Class that handles the image data and outputs it as a ros topic
//------------------------------------------------------------------------------------------------------------------------------
class CameraRosOutput
{
public:
    // Create a singleton instance of this class
    static CameraRosOutput* Create();
    // Destroy the instance
    void Destroy();
    // Initialize the instance
    Status Initialize(OutputRosInitData* pInitData);
    // Process the image data
    void ProcessImageData(camera_image_metadata_t* pImageMetadata, uint8_t* pImagePixels);

protected:
    // Disable direct instantiation of this class
    CameraRosOutput() {}
    ~CameraRosOutput() {}

private:
    static const int ImageQueueSize = 10;
    void Cleanup();

    ros::NodeHandle*                 m_pRosNodeHandle;      ///< ROS handle
    ros::Publisher                   m_publishImage;        ///< Image publisher
    image_transport::ImageTransport* m_pRosImageTransport;  ///< ROS Image transport
    image_transport::CameraPublisher m_rosImagePublisher;   ///< ROS Image publisher
    sensor_msgs::Image               m_imageMsg;            ///< Image message
    sensor_msgs::CameraInfo          m_cameraInfoMsg;       ///< Camera Info message
};

#endif // end #define OUTPUT_ROS_INTERFACE_H
