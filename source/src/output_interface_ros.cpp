// /*******************************************************************************
//  * Copyright 2020 ModalAI Inc.
//  *
//  * Redistribution and use in source and binary forms, with or without
//  * modification, are permitted provided that the following conditions are met:
//  *
//  * 1. Redistributions of source code must retain the above copyright notice,
//  *    this list of conditions and the following disclaimer.
//  *
//  * 2. Redistributions in binary form must reproduce the above copyright notice,
//  *    this list of conditions and the following disclaimer in the documentation
//  *    and/or other materials provided with the distribution.
//  *
//  * 3. Neither the name of the copyright holder nor the names of its contributors
//  *    may be used to endorse or promote products derived from this software
//  *    without specific prior written permission.
//  *
//  * 4. The Software is used solely in conjunction with devices provided by
//  *    ModalAI Inc.
//  *
//  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  * POSSIBILITY OF SUCH DAMAGE.
//  ******************************************************************************/

#include <opencv2/opencv.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "output_interface_ros.h"

///<@todo Remove this once the modal_camera header file is updated
#ifndef IMAGE_FORMAT_NV21
#define IMAGE_FORMAT_NV21 6
#endif

#ifndef IMAGE_FORMAT_JPG
#define IMAGE_FORMAT_JPG 7
#endif

//------------------------------------------------------------------------------------------------------------------------------
// Current time in nanoseconds
// @todo move to a utils file or some common place
//------------------------------------------------------------------------------------------------------------------------------
static int64_t CurrentTimeNanosecs()
{
    int64_t currentTime = -1;
    struct  timespec ts;

    if(clock_gettime(CLOCK_MONOTONIC, &ts))
    {
        LOG_ERROR("\n------ ERROR: calling clock_gettime");
    }
    else
    {
        currentTime = (int64_t)((int64_t)ts.tv_sec*1000000000 + (int64_t)ts.tv_nsec);
    }

    return currentTime;
}

//------------------------------------------------------------------------------------------------------------------------------
// Create an object of type CameraRosOutput and initialize it
//------------------------------------------------------------------------------------------------------------------------------
CameraRosOutput* CameraRosOutput::Create()
{
    return new CameraRosOutput;
}

//------------------------------------------------------------------------------------------------------------------------------
// Cleanup the object
//------------------------------------------------------------------------------------------------------------------------------
void CameraRosOutput::Cleanup()
{
    delete m_pRosNodeHandle;
    m_pRosNodeHandle = NULL;
}

//------------------------------------------------------------------------------------------------------------------------------
// Destroy the object
//------------------------------------------------------------------------------------------------------------------------------
void CameraRosOutput::Destroy()
{
    Cleanup();
    delete this;
}

//------------------------------------------------------------------------------------------------------------------------------
// Initialize the object
//------------------------------------------------------------------------------------------------------------------------------
Status CameraRosOutput::Initialize(OutputRosInitData* pInitData)
{
    m_pRosNodeHandle = pInitData->pRosNodeHandle;

    m_pRosImageTransport = new image_transport::ImageTransport(*m_pRosNodeHandle);
    m_rosImagePublisher  = m_pRosImageTransport->advertiseCamera(pInitData->pRosTopic, ImageQueueSize, false);

    LOG_INFO("\n----------- Output ros topic: %s %p %p", pInitData->pRosTopic, m_pRosNodeHandle, m_pRosImageTransport);

    return S_OK;
}

//------------------------------------------------------------------------------------------------------------------------------
// Process the image data coming from the camera server
//------------------------------------------------------------------------------------------------------------------------------
void CameraRosOutput::ProcessImageData(camera_image_metadata_t* pImageMetadata, uint8_t* pImagePixels)
{
    int64_t  frameTimeNsecs   = pImageMetadata->timestamp_ns;
    int64_t  currentTimeNsecs = CurrentTimeNanosecs();
    uint16_t width            = pImageMetadata->width;
    uint16_t height           = pImageMetadata->height;
    uint32_t stride           = pImageMetadata->stride;
    uint32_t sizeBytes        = pImageMetadata->size_bytes;

    if ((pImageMetadata->format == IMAGE_FORMAT_NV21) || (pImageMetadata->format == IMAGE_FORMAT_NV12))
    {
        // We will send the YUV image as RGB on the ros topic
        sizeBytes = (width * height * 3);
    }

    LOG_INFO("\n----------- Received Image Frame %d at current-time-nsecs: %lld frame-time-nsecs: %lld ... Diff-msecs %d",
              pImageMetadata->frame_id,
              currentTimeNsecs,
              frameTimeNsecs,
              (int)((currentTimeNsecs-frameTimeNsecs)/1000000));

    m_imageMsg.width           = width;
    m_imageMsg.height          = height;
    m_imageMsg.step            = stride;
    m_imageMsg.is_bigendian    = 0;
    m_imageMsg.header.frame_id = std::string("voxl_mpa_cam_ros");

    m_imageMsg.header.stamp.fromNSec(pImageMetadata->timestamp_ns);
    m_imageMsg.data.resize(sizeBytes);

    m_cameraInfoMsg.header.frame_id  = std::string("voxl_mpa_cam_ros");
    m_cameraInfoMsg.width            = width;
    m_cameraInfoMsg.height           = height;
    m_cameraInfoMsg.header.stamp.fromNSec(pImageMetadata->timestamp_ns);

    ///<@todo read this from config file
    // distortion parameters. 5-param radtan model identical to factory sensor cal
    // ------------------------------------------------ Calibration parameters -------------------------------------------------
    // These are fixed during camera calibration. Their values will be the same in all messages until the camera is
    // recalibrated. The internal parameters can be used to warp a raw (distorted) image to:
    //   1. An undistorted image (requires D and K)
    //   2. A rectified image (requires D, K, R)
    //
    // The projection matrix P projects 3D points into the rectified image
    // -------------------------------------------------------------------------------------------------------------------------
    m_cameraInfoMsg.distortion_model = "plumb_bob";
    m_cameraInfoMsg.D.resize(5);
    m_cameraInfoMsg.D[0] = 100.01;
    m_cameraInfoMsg.D[1] = 100.01;
    m_cameraInfoMsg.D[2] = 100.01;
    m_cameraInfoMsg.D[3] = 100.01;
    m_cameraInfoMsg.D[4] = 100.01;

    // Intrinsic camera matrix for the raw (distorted) images.
    m_cameraInfoMsg.K[0] = 100.01;
    m_cameraInfoMsg.K[1] = 0;
    m_cameraInfoMsg.K[2] = 100.01;
    m_cameraInfoMsg.K[3] = 0;
    m_cameraInfoMsg.K[4] = 100.01;
    m_cameraInfoMsg.K[5] = 100.01;
    m_cameraInfoMsg.K[6] = 0;
    m_cameraInfoMsg.K[7] = 0;
    m_cameraInfoMsg.K[8] = 1;

    // Rectification matrix STEREO CAM ONLY, leave as identity for monocular
    m_cameraInfoMsg.R[0] = 1;
    m_cameraInfoMsg.R[1] = 0;
    m_cameraInfoMsg.R[2] = 0;
    m_cameraInfoMsg.R[3] = 0;
    m_cameraInfoMsg.R[4] = 1;
    m_cameraInfoMsg.R[5] = 0;
    m_cameraInfoMsg.R[6] = 0;
    m_cameraInfoMsg.R[7] = 0;
    m_cameraInfoMsg.R[8] = 1;

    // Projection matrix
    m_cameraInfoMsg.P[0] = 100.01;
    m_cameraInfoMsg.P[1] = 0;
    m_cameraInfoMsg.P[2] = 100.01;
    m_cameraInfoMsg.P[3] = 0;  // Tx for stereo offset
    m_cameraInfoMsg.P[4] = 0;
    m_cameraInfoMsg.P[5] = 100.01;
    m_cameraInfoMsg.P[6] = 100.01;
    m_cameraInfoMsg.P[7] = 0;  // Ty for stereo offset
    m_cameraInfoMsg.P[8] = 0;
    m_cameraInfoMsg.P[9] = 0;
    m_cameraInfoMsg.P[10] = 1;
    m_cameraInfoMsg.P[11] = 0;

    if (pImageMetadata->format == IMAGE_FORMAT_RAW8)
    {
        m_imageMsg.encoding = std::string("mono8");
        m_imageMsg.step     = width;

        memcpy(&m_imageMsg.data[0], pImagePixels, sizeBytes);
    }
    else if (pImageMetadata->format == IMAGE_FORMAT_NV21)
    {
        cv::Mat rgbImage = cv::Mat();
        cv::Mat nv21(height + height/2, width, CV_8UC1, (uchar*)pImagePixels);
        cv::cvtColor(nv21, rgbImage, CV_YUV2RGB_NV21);

        m_imageMsg.encoding = std::string("rgb8");
        m_imageMsg.step     = width*3; ///<@todo handle this correctly

        fprintf(stderr, "\n------Received NV21 frame: %d %d %d", width, height, (m_imageMsg.step * m_imageMsg.height));

        memcpy(&m_imageMsg.data[0], (void*)rgbImage.data, sizeBytes);
    }
    else ///<@todo Assuming RGB8
    {
        m_imageMsg.encoding = std::string("rgb8");
        m_imageMsg.step     = width*3; ///<@todo handle this correctly

        memcpy(&m_imageMsg.data[0], pImagePixels, sizeBytes);
    }

    m_rosImagePublisher.publish(m_imageMsg, m_cameraInfoMsg);
}
