# voxl-mpa-cam-ros

This document explains how to write a ROS client for the voxl-camera-server. The client listens to the camera frame output from the server and emits ROS image messages. These topics can be viewed in rviz.

High Level Overview
===================
The following picture shows at a glance how the different components interact with each other.

![](images/architecture.png)

Build instructions
==================
1. (PC) Requires the voxl-emulator (found [here](https://gitlab.com/voxl-public/voxl-docker))
1. (VOXL) Requires WiFi setup on VOXL, instructions can be found [here](https://docs.modalai.com/wifi-setup/)
1. (PC) git clone git@gitlab.com:voxl-public/voxl-mpa-cam-ros.git
1. (PC) cd voxl-mpa-cam-ros
1. (PC) sudo voxl-docker -i voxl-emulator
1. (VOXL-EMULATOR) source /opt/ros/indigo/setup.bash
1. (VOXL-EMULATOR) ./install_build_deps.sh
1. (VOXL-EMULATOR) ./clean.sh
1. (VOXL-EMULATOR) ./build.sh
    - With a successful build you'll see:
    - [100%] Linking CXX executable /home/root/devel/lib/voxl_mpa_cam_ros/voxl_mpa_cam_ros_node
    - [100%] Built target voxl_mpa_cam_ros_node
    - Install the project...
    - followed by some other information
1. (VOXL-EMULATOR) ./make_package.sh    

Steps to run
============
1. (VOXL) Requires voxl-camera-server found [here](https://gitlab.com/voxl-public/voxl-camera-server)
1. (PC) Open a terminal window
1. (PC) cd path-to/voxl-mpa-cam-ros
1. (PC) ./install_on_voxl.sh
1. (PC) adb shell
1. (VOXL) source /opt/ros/indigo/setup.bash 2>/dev/null
1. (VOXL) ifconfig
    - Note the IP address
1. (VOXL) export ROS_IP=IP-ADDRESS-OF-VOXL
1. (VOXL) roscore
    - Note down the string that reads something like "ROS_MASTER_URI=http://xxx.yyy.zzz.www:abcde/"
    - The http address will be different
1. (PC) source /opt/ros/kinetic/setup.bash    
1. (PC) ifconfig
    - Note the IP address
1. (PC) export ROS_IP=IP-ADDRESS-OF-PC
1. (PC) export ROS_MASTER_URI=http://xxx.yyy.zzz.www:abcde/
    - ROS_MASTER_URI is what we saw when we started roscore above
1. (PC) rviz
    - Click on "Add" at the bottom-left
    - Select "Image"
    - Change Display Name to "My-Camera-Image"
    - On the left column expand the "My-Camera-Image"
    - Click on the "Image Topic" and in the right column enter "/voxl_hires_image" 
1. (PC) Open another terminal window
1. (PC) adb shell
1. (VOXL) voxl-camera-server -c /etc/modalai/voxl-camera-server.conf
1. (PC) Open one more terminal window
1. (PC) adb shell
1. (VOXL) source /opt/ros/indigo/setup.bash 2>/dev/null
1. (VOXL) export ROS_IP=IP-ADDRESS-OF-VOXL
1. (VOXL) export ROS_MASTER_URI=http://xxx.yyy.zzz.www:abcde/
    - ROS_MASTER_URI is what we saw when we started roscore above
1. (VOXL) python /usr/bin/launch_voxl_mpa_cam_ros.py
1. (PC) You should see the image streaming to your PC in rviz